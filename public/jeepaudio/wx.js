
var ApiList = [
    'onMenuShareTimeline',
    'onMenuShareAppMessage',
    'onMenuShareQQ',
    'onMenuShareWeibo',
    'onMenuShareQZone',
    'startRecord',
    'stopRecord',
    'onVoiceRecordEnd',
    'playVoice',
    'pauseVoice',
    'stopVoice',
    'onVoicePlayEnd',
    'uploadVoice'
];

var share_data = {
    title:'多乐士萌宠教你春节财运双收',
    desc:'新年第一吉祥汪，教你春节财运双收',
    friends_title:'多乐士萌宠教你用表情包玩转春节',
    link:location.href,
    img:"http://wx.zhijin101.com/dlsexpression/images/icon.jpg"
};

function wx_share(data, callback) {
    var appMessage = {
        title: data.title,                          //必填,分享标题
        desc: data.desc,                         //选填,分享描述
        imgUrl: data.img,                       //选填,分享图片
        link: data.link,   //必填,支持直接填写location.href
        success: function () {
          // 用户确认分享后执行的回调函数

        },
        cancel: function () {
           // 用户取消分享后执行的回调函数
        }
    };
    var timeline = { 
        title: data.friends_title,                //必填,分享标题
        desc: data.desc,                         //选填,分享描述
        imgUrl: data.img,                       //选填,分享图片
        link: data.link,   //必填,支持直接填写location.href
        success: function () {
          // 用户确认分享后执行的回调函数

        },
        cancel: function () {
           // 用户取消分享后执行的回调函数
        }
    };
    wx.onMenuShareAppMessage(tar.shapeShareAppMessage(appMessage));
    wx.onMenuShareTimeline(tar.shapeShareTimeline(timeline));
}

var shareCallback = function (type, status) {
    
}

var wxConfigCallback = function() {
    // wx_share(share_data, shareCallback);
}

function getWxConfig(ApiList, callback, debug) {
    $.ajax({
        url: 'http://wx.zhijin101.com/index.php/Index/api/get_JSAPI_Config',
        type: 'POST',
        dataType: 'json',
        data: {'url':location.href},
    }).done(function(res) {
        if(res.s){
            var con = res.d;
            var wxConf = {
                debug : debug,
                appId: con.appid,
                timestamp: con.timestamp,
                nonceStr: con.noncestr,
                signature: con.signature,
                jsApiList:ApiList
            };
            wx.config(wxConf);
            wx.ready(function(){
                callback();
            });
        }
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}
getWxConfig(ApiList, wxConfigCallback, true);