
// var ApiList = [
//     'onMenuShareTimeline',
//     'onMenuShareAppMessage'
// ];


// function wx_share(data, callback) {

//     wx.onMenuShareTimeline({
//         title: data.title, // 分享标题
//         link: data.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
//         imgUrl: data.img, // 分享图标
//         success: function () {
//             // 用户确认分享后执行的回调函数
//             if(typeof(callback) != 'undefined'){
//                 callback('sharetimeline', 1);
//             }
//         },
//         cancel: function () {
//             // 用户取消分享后执行的回+调函数
//             if(typeof(callback) != 'undefined'){
//                 callback('sharetimeline', 0);
//             }
//         }
//     });

//     wx.onMenuShareAppMessage({
//         title: data.title, // 分享标题
//         desc: data.desc, // 分享描述
//         link: data.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
//         imgUrl: data.img, // 分享图标
//         type: '', // 分享类型,music、video或link，不填默认为link
//         dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
//         success: function () { 
//             // 用户确认分享后执行的回调函数
//             if(typeof(callback) != 'undefined'){
//                 callback('shareappmessage', 1);
//             }
//         },
//         cancel: function () { 
//             // 用户取消分享后执行的回调函数
//             if(typeof(callback) != 'undefined'){
//                 callback('shareappmessage', 0);
//             }
//         }
//     });


//     // var appMessage = {
//     //     title: data.title,                          //必填,分享标题
//     //     desc: data.desc,                         //选填,分享描述
//     //     imgUrl: data.img,                       //选填,分享图片
//     //     link: data.link,   //必填,支持直接填写location.href
//     //     success: function () {
//     //       // 用户确认分享后执行的回调函数

//     //     },
//     //     cancel: function () {
//     //        // 用户取消分享后执行的回调函数
//     //     }
//     // };
//     // var timeline = { 
//     //     title: data.friends_title,                //必填,分享标题
//     //     desc: data.desc,                         //选填,分享描述
//     //     imgUrl: data.img,                       //选填,分享图片
//     //     link: data.link,   //必填,支持直接填写location.href
//     //     success: function () {
//     //       // 用户确认分享后执行的回调函数

//     //     },
//     //     cancel: function () {
//     //        // 用户取消分享后执行的回调函数
//     //     }
//     // };
//     // wx.onMenuShareAppMessage(tar.shapeShareAppMessage(appMessage));
//     // wx.onMenuShareTimeline(tar.shapeShareTimeline(timeline));
// }

// var shareCallback = function (type, status) {
    
// }

function getCookie(name){
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg)){
        return unescape(arr[2]);
    }else{
        return null;
    }
}

function getWxConfig(ApiList, callback, debug) {
    $.ajax({
        url: 'http://wx.zhijin101.com/index.php/Index/api/get_JSAPI_Config',
        type: 'POST',
        dataType: 'json',
        data: {'url':location.href},
    }).done(function(res) {
        if(res.s){
            var con = res.d;
            var wxConf = {
                debug : debug,
                appId: con.appid,
                timestamp: con.timestamp,
                nonceStr: con.noncestr,
                signature: con.signature,
                jsApiList:ApiList
            };
            wx.config(wxConf);
            wx.ready(function(){
                callback();
            });
        }
    }).fail(function() {
        console.log("error");
    }).always(function() {
        console.log("complete");
    });
}
