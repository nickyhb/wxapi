(function () {
    function w() {
        var r = document.documentElement;
        var a = r.getBoundingClientRect().width;
        if (a > 750) {
            a = 750;
        }
        //750/w = 100/font-size
        rem = a / 7.5;
        r.style.fontSize = rem + "px"
    }

    var t;
    w();
    window.addEventListener("resize", function () {
        clearTimeout(t);
        t = setTimeout(w, 300)
    }, false);
})();

// 百度统计
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?bc36839c42db64ed6126f7421e59e292";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();



var qian_arr = [
    // 健康
    {
        img:'jiankangqian.png',
        list:[
            {
                info:'jian1.png',// 文章图片
                qi:'qi1.png'// 底部油漆图片
            },
            {
                info:'jian2.png',// 文章图片
                qi:'qi2.png'// 底部油漆图片
            },
            {
                info:'jian3.png',// 文章图片
                qi:'qi3.png'// 底部油漆图片
            },
            {
                info:'jian4.png',// 文章图片
                qi:'qi4.png'// 底部油漆图片
            },     {
                info:'jian5.png',// 文章图片
                qi:'qi5.png'// 底部油漆图片
            }
        ]
    },
    //财运
    {
        img:'caifuqian.png',
        list:[
            {
                info:'cai1.png',// 文章图片
                qi:'qi1.png'// 底部油漆图片
            },
            {
                info:'cai2.png',// 文章图片
                qi:'qi2.png'// 底部油漆图片
            },
            {
                info:'cai3.png',// 文章图片
                qi:'qi3.png'// 底部油漆图片
            },
            {
                info:'cai4.png',// 文章图片
                qi:'qi4.png'// 底部油漆图片
            },
            {
                info:'cai5.png',// 文章图片
                qi:'qi5.png'// 底部油漆图片
            }
        ]
    },
    //爱情
    {
        img:'aiqingqian.png',
        list:[
            {
                info:'ai1.png',// 文章图片
                qi:'qi1.png'// 底部油漆图片
            },
            {
                info:'ai2.png',// 文章图片
                qi:'qi2.png'// 底部油漆图片
            },
            {
                info:'ai3.png',// 文章图片
                qi:'qi3.png'// 底部油漆图片
            },
            {
                info:'ai4.png',// 文章图片
                qi:'qi4.png'// 底部油漆图片
            },
            {
                info:'ai5.png',// 文章图片
                qi:'qi5.png'// 底部油漆图片
            }
        ]
    },
    //事业
    {
        img:'shiyeqian.png',
        list:[
            {
                info:'shi1.png',// 文章图片
                qi:'qi1.png'// 底部油漆图片
            },
            {
                info:'shi2.png',// 文章图片
                qi:'qi2.png'// 底部油漆图片
            },
            {
                info:'shi3.png',// 文章图片
                qi:'qi3.png'// 底部油漆图片
            },
            {
                info:'shi4.png',// 文章图片
                qi:'qi4.png'// 底部油漆图片
            },      {
                info:'shi5.png',// 文章图片
                qi:'qi5.png'// 底部油漆图片
            }
        ]
    }
];

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}




















