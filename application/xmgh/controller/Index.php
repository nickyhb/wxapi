<?php
namespace app\xmgh\controller;

use app\xmgh\controller\Base;

/**
 *
 */
class Index extends Base {


	public function index() {
		$share_title = '厦一站·汇团圆 x 智行：壹元回家计划';
		$share_desc = '这个春节，我想带你一起回家。';
		$user_lset_model = db('xmgh_user_lset');
		if($this->user_id){
			$info = $user_lset_model->where(['user_id' => $this->user_id])->order('id desc')->find();
			if ($info) {
				$lset_info = db('xmgh_lset')->where(['id' => $info['lset_id']])->find();
				if ($lset_info) {
					$share_title = '我已成功为来自'.$lset_info['hometown'].'的留守儿童'.$lset_info['user_name'].'父母提供1元回家基金，请你一起加入！';
					$share_desc = '和我一起加入厦一站·汇团圆 x 智行#壹元回家计划#，助力更多留守家庭实现新春团圆！';
				}
			}
		}
		$this->assign('share_title', $share_title);
		$this->assign('share_desc', $share_desc);
		// dump(config('view_replace_str'));die;
		return $this->fetch('', [], config('view_replace_str'));
	}
	public function bind_lset($user_id = 0) {
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Headers: X-Requested-With,X_Requested_With');
		header("Content-type: text/json; charset=utf-8");
		// if($this->platform!='weixin'){
		// 	return json(['status' => 0, 'msg' => '请在微信打开']);
		// }
		$this->user_id = $this->user_id ? $this->user_id : 0;
		$lset_info = parent::bind_lset($this->user_id);
		$total_num = db('xmgh_user_lset')->count();
		$lset_info['total_num'] = $total_num;

		return json(['status' => 1, 'info' => $lset_info]);
	}

	public function adduser() {
		$data = input('data');
		trace($data);
		if ($data) {
			try {
				$wx_user = json_decode(base64_decode($data), true);
				trace($wx_user);
				if (is_array($wx_user)) {
					$wx_user['openid'] = $wx_user['uid'];
					$wx_user['tel'] = $wx_user['phone'];
					$wx_user['nickname'] = substr_replace($wx_user['phone'], '****' , 3, 4);;
					$res = $this->add_user($wx_user, 'zx');
					if ($res['status']) {
						cookie('user_id', $res['user_id'], $this->cookie_expire);
					}
				}
			} catch (\Exception $e) {
			}
		}
		$targetUrl = url('index/index', [], true, true);
		$this->redirect($targetUrl);
	}

	public function clear($value = '') {
		cookie('user_id', null);
		return '用户清除成功';
	}
}