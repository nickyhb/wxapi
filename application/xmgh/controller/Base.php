<?php
namespace app\xmgh\controller;

use think\Controller;

/**
* 
*/
class Base extends Controller
{
    public $user;
    public $user_id;
    public $lset_info;
    public $platform;

    protected function _initialize(){
        $this->current_url = current_url();   
        // cookie超时时间
        $this->cookie_expire = 86400 * 7;
        $user_id = cookie('user_id');
        if($user_id){
            $this->user = db('user')->where(['id'=>$user_id])->find();
        }

        $view_config = [
            '__HTML__'      => '/xmgh/',
            '__HTML_CSS__'  => '/xmgh/css',
            '__HTML_FONT__' => '/xmgh/font',
            '__HTML_IMG__'  => '/xmgh/images',
            '__HTML_LSET__' => '/xmgh/lset',
            '__HTML_JS__'   => '/xmgh/js',
        ];
        config('view_replace_str', array_merge(config('view_replace_str'), $view_config));
        $this->platform = get_platform();
        switch ($this->platform) {
            case 'weixin':
                if(empty($this->user)){
                    $this->wxauth();
                }
                break;
        }
        $this->user_id = $user_id;
        $this->user['nickname'] = isset($this->user['nickname']) ? base64_decode($this->user['nickname']) : '';
        $this->assign('user', $this->user);
        $this->assign('user_id', $user_id);
        $this->assign('platform', $this->platform);
    }

    public function wxauth()
    {
        $action = $this->request->action();
        trace($this->current_url);
        trace($action);
        if(!in_array(strtolower($action), ['oauth_callback', 'adduser'])){
            cookie('target_url', $this->current_url, $this->cookie_expire);
            $callback = urlencode(url('Base/oauth_callback', [], true, true));
            $ourl = "http://wx.zhijin101.com/index.php/index/api/oauth".'?callback=' .$callback. '&type=userinfo';
            $this->redirect($ourl);
            die;  
        }
    }

    public function oauth_callback()
    {
        $wechat_userinfo = input('post.data');
        if (!empty($wechat_userinfo)) {
            $wechat_userinfo = json_decode(base64_decode($wechat_userinfo), true);
            if (!empty($wechat_userinfo['openid'])) {
                $res = $this->add_user($wechat_userinfo);
                if($res['status']){
                    cookie('user_id', $res['user_id'], $this->cookie_expire);
                }

                // $openid = $wechat_userinfo['openid'];
                // $project = 'xmgh';
                // $user_model = db('user');
                // $has = $user_model->where(['openid' => $openid, 'project' => $project])->find();
                // $data['openid'] = $openid;
                // if (isset($wechat_userinfo['nickname'])) {
                //     $nickname = isset($wechat_userinfo['nickname']) ? $wechat_userinfo['nickname'] : '';
                //     $data['nickname'] = base64_encode($nickname);
                //     $data['wx_name'] = $nickname;
                // }
                // if (isset($wechat_userinfo['sex'])) {
                //     $sex = isset($wechat_userinfo['sex']) ? $wechat_userinfo['sex'] : '';
                //     $data['sex'] = $sex;
                // }
                // if (isset($wechat_userinfo['headimgurl'])) {
                //     $headimgurl = isset($wechat_userinfo['headimgurl']) ? $wechat_userinfo['headimgurl'] : '';
                //     $data['wx_avatar'] = $headimgurl;
                // }
                // $time = time();
                // $data['update_time'] = $time;

                // if (empty($has)) {
                //     $data['avatar'] = 0;
                //     $data['project'] = $project;
                //     $data['create_time'] = $time;

                //     $res = $user_model->insert($data);
                //     $user_id = $user_model->getLastInsID();
                // } else {
                //     $res = $user_model->where(['id' => $has['id']])->update($data);
                //     $user_id = $has['id'];
                // }
                // // $this->bind_lset($user_id);
                // cookie('user_id', $user_id, $this->cookie_expire);
            }
        }
        $targetUrl = empty(cookie('target_url')) ? url('index/index/index') : cookie('target_url');
        $this->redirect($targetUrl);
    }

    public function add_user($wx_user, $project='xmgh')
    {
        if(!isset($wx_user['openid'])){
            return ['status'=>0];
        }
        $openid = $wx_user['openid'];
        $user_model = db('user');
        $has = $user_model->where(['openid' => $openid, 'project' => $project])->find();
        $data['openid'] = $openid;
        if (isset($wx_user['nickname'])) {
            $data['nickname'] = base64_encode($wx_user['nickname']);
            $data['wx_name'] = $wx_user['nickname'];
        }
        if (isset($wx_user['sex'])) {
            $sex = isset($wx_user['sex']) ? $wx_user['sex'] : '';
            $data['sex'] = $sex;
        }
        if (isset($wx_user['tel'])) {
            $data['tel'] = isset($wx_user['tel']) ? $wx_user['tel'] : '';
        }
        if (isset($wx_user['headimgurl'])) {
            $headimgurl = isset($wx_user['headimgurl']) ? $wx_user['headimgurl'] : '';
            $data['wx_avatar'] = $headimgurl;
        }
        $time = time();
        $data['update_time'] = $time;

        if (empty($has)) {
            $data['avatar'] = 0;
            $data['project'] = $project;
            $data['create_time'] = $time;

            $res = $user_model->insert($data);
            $user_id = $user_model->getLastInsID();
        } else {
            $res = $user_model->where(['id' => $has['id']])->update($data);
            $user_id = $has['id'];
        }
        return ['status'=>1, 'user_id'=>$user_id];
    }

    public function bind_lset($user_id = 0)
    {
        $user_lset_model = db('xmgh_user_lset');
        $info = $user_lset_model->where(['user_id'=>$user_id])->order('id desc')->find();
        $lset_info = [];
        if($info && $user_id){
            $lset_id = $info['lset_id'];
        }else{
            $lset_id = $this->get_rand_lset_id();
            $user_lset_model->insert([
                'user_id'       => $user_id,
                'lset_id'       => $lset_id,
                'create_time'   => time()
            ]);
        }
        $lset_info = db('xmgh_lset')->where(['id'=>$lset_id])->find();
        return $lset_info;
    }

    public function get_lset_ids()
    {
        return db('xmgh_lset')->column('id');
    }

    public function get_rand_lset_id()
    {
        $arr_ids = $this->get_lset_ids();
        return $arr_ids[array_rand($arr_ids)];
    }
}