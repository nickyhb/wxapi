<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // 'log'                    => [
    //     // 日志记录方式，内置 file socket 支持扩展
    //     'type'  => 'Test',
    //     // 日志保存目录
    //     'path'  => LOG_PATH,
    //     // 日志记录级别
    //     'level' => [],
    // ],

    // Socket调试
    'log' =>  [
        'type'                => 'socket',
        'host'                => 'slog.thinkphp.cn',
        //日志强制记录到配置的client_id
        'force_client_ids'    => ['slog_69ecb0', 'slog_69ecb0'],
        //限制允许读取日志的client_id
        'allow_client_ids'    => ['slog_69ecb0', 'slog_69ecb0'],
    ], 
];
