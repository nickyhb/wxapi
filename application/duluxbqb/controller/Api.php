<?php

namespace app\duluxbqb\controller;

use think\Controller;

/**
 * 
 * @package app\duluxbqb\controller
 */
class Api extends Controller {

    public function oauthback(){
        $openid = input('openid');
        $model = db('user');
        if($openid){
            $has = $model->where(['openid'=>$openid])->find();
            if(!$has){
                $model->insert(['openid'=>$openid, 'create_time'=>time()]);
                $id = $model->getLastInsID();
                if($id){
                    return json(['status'=>1]);
                }
            }else{
                return json(['status'=>1]);
            }
        }
        return json(['status'=>0]);
    }

    public function record()
    {
        $openid = input('openid');
        $data = [
            'openid'        => $openid,
            'create_time'   => time(),
        ];
        $res = db('enter')->insert($data);
        if($res){
            return json(['status'=>1]);
        }else{
            return json(['status'=>0]);
        }
    }

    public function record_num()
    {
        $num = db('enter')->count();
        return '<span style="font-size:100px;color:#000;">'.$num.'</span>';
    }
}