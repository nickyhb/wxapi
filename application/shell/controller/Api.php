<?php

namespace app\shell\controller;

use think\Controller;

/**
 * 
 * @package app\shell\controller
 */
class Api extends Controller {

    public $openid;
    public $user_id;
    public $platform;

    protected function _initialize(){
        $this->current_url = current_url();
        // cookie超时时间
        $this->cookie_expire = 86400 * 7;
        $openid = cookie('openid');
        if($openid){
            $this->user = db('user')->where(['openid'=>$openid])->find();
        }
        if(empty($this->user)){
            $this->wxauth();
        }
    }

    public function oauth()
    {
        $this->redirect('http://wx.zhijin101.com/shell/index.html');
        die;
    }

    public function clear_user()
    {
        cookie('openid', null);
        cookie('nickname', null);
        cookie('headimgurl', null);
        return '清除成功';
    }

    public function wxauth()
    {
        $action = $this->request->action();
        if(!in_array(strtolower($action), ['oauth_callback', 'oauthback'])){
            cookie('target_url', $this->current_url, $this->cookie_expire);
            $callback = urlencode(url('Api/oauth_callback', [], true, true));
            $ourl = "http://wx.zhijin101.com/index.php/index/api/oauth".'?callback=' .$callback. '&type=userinfo';
            $this->redirect($ourl);
            die;  
        }
    }

    public function oauth_callback()
    {
        $wechat_userinfo = input('post.data');
        if (!empty($wechat_userinfo)) {
            $wechat_userinfo = json_decode(base64_decode($wechat_userinfo), true);
            if (!empty($wechat_userinfo['openid'])) {
                $res = $this->add_user($wechat_userinfo);
                if($res['status']){
                    cookie('openid', $res['openid'], $this->cookie_expire);
                    cookie('nickname', $res['user']['wx_name'], $this->cookie_expire);
                    cookie('headimgurl', $res['user']['wx_avatar'], $this->cookie_expire);
                }
            }
        }
        $targetUrl = empty(cookie('target_url')) ? url('Api/oauth') : cookie('target_url');
        $this->redirect($targetUrl);
    }

    private function add_user($wx_user)
    {
        if(!isset($wx_user['openid'])){
            return ['status'=>0];
        }
        $openid = $wx_user['openid'];
        $user_model = db('user');
        $has = $user_model->where(['openid' => $openid])->find();
        $data = [];
        $data['openid'] = $openid;
        if (isset($wx_user['nickname'])) {
            $data['nickname'] = base64_encode($wx_user['nickname']);
            $data['wx_name'] = $wx_user['nickname'];
        }
        if (isset($wx_user['sex'])) {
            $sex = isset($wx_user['sex']) ? $wx_user['sex'] : '';
            $data['sex'] = $sex;
        }
        if (isset($wx_user['tel'])) {
            $data['tel'] = isset($wx_user['tel']) ? $wx_user['tel'] : '';
        }
        if (isset($wx_user['headimgurl'])) {
            $headimgurl = isset($wx_user['headimgurl']) ? $wx_user['headimgurl'] : '';
            $data['wx_avatar'] = $headimgurl;
        }
        $time = time();
        $data['update_time'] = $time;

        if (empty($has)) {
            $data['create_time'] = $time;

            $res = $user_model->insert($data);
            $user_id = $user_model->getLastInsID();
        } else {
            $res = $user_model->where(['id' => $has['id']])->update($data);
            $user_id = $has['id'];
        }
        return ['status'=>1, 'user_id'=>$user_id, 'user'=>$data, 'openid'=>$openid];
    }

    public function get_nickname()
    {
        $openid = input('openid');
        $has = db('user')->where(['openid' => $openid])->find();
        if($has){
            return json(['status'=>1, 'name'=>base64_decode($has['nickname'])]);
        }else{
            return json(['status'=>0, 'name'=>'']);
        }
    }

    public function oauthback(){
        $openid = input('openid');
        $model = db('hb_user');
        if($openid){
            $has = $model->where(['openid'=>$openid])->find();
            if(!$has){
                $model->insert(['openid'=>$openid, 'create_time'=>time()]);
                $id = $model->getLastInsID();
                if($id){
                    return json(['status'=>1]);
                }
            }else{
                return json(['status'=>1]);
            }
        }
        return json(['status'=>0]);
    }
}