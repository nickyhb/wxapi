<?php

mb_internal_encoding('UTF-8');

// 变量调试
function _P($data,$isEnd = true)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    if($isEnd) exit();
}

function _A($s=true,$m="success",$d=array()){
	header("Access-Control-Allow-Origin:*");
	header("Access-Control-Allow-Headers:X-Requested-With");
	header('Content-type: text/json');
	exit(json_encode(array(
		's'	=>	$s,
		'm'	=>	$m,
		'd'	=>	$d
	)));
}


// 获取已经安装的模块
function get_app($name=''){
    static $apps;
    if (!$apps && !$apps = \think\Cache::get('eb_apps')) {
        $apps = \think\Db::name('app') -> where('status',1) -> column(true,'name');
        \think\Cache::set('eb_apps',$apps);
    }
    if ($name) {
        return isset($apps[$name])?$apps[$name]:false;
    }else{
        return $apps;
    }
}
// 判断是否安装app
function check_app($name=''){
    $name = strtolower($name);
    // 排除三个内置模块
    $not_check = ['admin','index'];
    if (in_array($name, $not_check)) {
        return true;
    }
    if ($name && $app = get_app($name)) {
        if ($app['status']) {
            return true;
        }
    }
    return false;
}

function is_login(){
    return \think\Session::get('user_id') ? true : false;
}

function user($field=null){
    static $user='';
    if ('' === $user) {
        $user = \app\user\model\User::get(\think\Session::get('user_id')?:0);
    }
    return $field?$user[$field]:$user;
}

function get_root($domain = false)
{
    $str = dirname(request()->baseFile());
    $str = ($str == DS) ? '' : $str;
    return $domain ? request()->domain() . $str : $str;
}

// 检查权限
function check_auth($action, $controller = '', $module = ''){
    if (\think\Session::get('super_admin')) {
        return true;
    }
    static $auth;
    if (!$auth) {
        $prefix = \think\Config::get('database.prefix');
        $config = [
            'AUTH_GROUP' => $prefix . 'auth_group',
            'AUTH_ACCESS' => $prefix . 'auth_access',
            'AUTH_RULE' => $prefix . 'auth_rule',
            'AUTH_USER' => $prefix . 'manager',
            'AUTH_ON' => true,
            'AUTH_TYPE' => \ebcms\Config::get('system.base.auth_type'),
        ];
        $auth = new \ebcms\Auth($config);
    }
    $module = $module ?: request()->module();
    $controller = $controller ?: request()->controller();
    $action = $action ?: request()->action();
    $node = strtolower($module . '_' . $controller . '_' . $action);
    if ($auth->check($node, \think\Session::get('manager_id'))) {
        return true;
    }
    return false;
}

function eb_config($name)
{
    return \ebcms\Config::get($name);
}

// 获取缩略图真实地址
function thumb($file, $width = 0, $height = 0, $type = 3)
{
    if (strpos($file, '://')) {
        return $file;
    }
    $base = request() -> root();
    if (!$width || !$height) {
        if (is_file('./upload' . $file)) {
            return $base . '/upload' . $file;
        }
        return $base . '/system/image/nopic.gif';
    } else {
        $res = $base . '/upload' . $file . '!' . $width . '_' . $height . '_' . $type . '.' . pathinfo($file, PATHINFO_EXTENSION);
        $thumbfile = './upload' . $file . '!' . $width . '_' . $height . '_' . $type . '.' . pathinfo($file, PATHINFO_EXTENSION);
        $file = './upload' . $file;
    }
    if (!is_file($thumbfile)) {
        if (!is_file($file)) {
            return $base . '/system/image/nopic.gif';
        } else {
            \think\Image::open($file)->thumb($width, $height, $type)->save($thumbfile, null, 100);
        }
    }
    return $res;
}

// 获取缩略图真实地址
function file_url($file)
{
    return strpos($file, '://') ? $file : request() -> root() . '/upload' . $file;
}


/**
 * 当前域名
 */
if(!function_exists('current_host')){
    function current_host(){
        return (is_ssl() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
    }
}

/**
 * 当前地址
 */
if(!function_exists('current_url')){
    function current_url(){
        return (is_ssl() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
}
/**
 * 检查是不是ssl协议
 */
if(!function_exists('is_ssl')){
    function is_ssl(){
        $server = $_SERVER;
        if (isset($server['HTTPS']) && ('1' == $server['HTTPS'] || 'on' == strtolower($server['HTTPS']))) {
            return true;
        } elseif (isset($server['REQUEST_SCHEME']) && 'https' == $server['REQUEST_SCHEME']) {
            return true;
        } elseif (isset($server['SERVER_PORT']) && ('443' == $server['SERVER_PORT'])) {
            return true;
        } elseif (isset($server['HTTP_X_FORWARDED_PROTO']) && 'https' == $server['HTTP_X_FORWARDED_PROTO']) {
            return true;
        }
        return false;
    }
}


/**
 * 获取访问的平台
 */
if (!function_exists('get_platform')) {
    function get_platform(){
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) { 
            return 'weixin'; 
        } else if (is_mobile()) {
            return 'mobile';
        } else {
            return 'pc';
        }
    }
}

/**
 * 是否手机访问
 * @return bool
 */
if (!function_exists('is_mobile')) {
    function is_mobile(){
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }
        // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset($_SERVER['HTTP_VIA'])) {
            // 找不到为flase,否则为true
            return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
        }
        // 脑残法，判断手机发送的客户端标志,兼容性有待提高
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array('nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile',);
            // 从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }
        // 协议法，因为有可能不准确，放到最后判断
        if (isset($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }
        return false;
    }
}